﻿namespace WebCasas.Business.Entities.Interfaces
{
    public interface IMaterial
    {
        string MaterialName { get; set; }
        bool HasMetal { get; set; }
        bool HasWood { get; set; }
    }
}
