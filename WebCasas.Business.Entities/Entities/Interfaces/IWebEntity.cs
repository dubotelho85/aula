﻿namespace WebCasas.Business.Entities.Interfaces
{
    public interface IWebEntity
    {
         long Id { get; set; }
    }
}
