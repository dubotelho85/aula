﻿namespace WebCasas.Business.Entities.Interfaces
{
    public interface IDimensions
    {
        int Width { get; set; }
        int Height { get; set; }
    }
}
