﻿using System.Collections.Generic;
using WebCasas.Business.Entities.Interfaces;

namespace WebCasas.Business.Entities
{
    public class Home : IWebEntity
    {
        public long Id { get; set; }
        public Floor Floor { get; set; }
        public IEnumerable<Door> Doors { get; set; }
        public IEnumerable<Window> Windows { get; set; }
    }
}
