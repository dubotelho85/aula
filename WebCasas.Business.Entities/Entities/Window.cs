﻿using WebCasas.Business.Entities.Interfaces;

namespace WebCasas.Business.Entities
{
    public class Window : IWebEntity, IDimensions, IMaterial
    {
        public long Id { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string MaterialName { get; set; }
        public bool HasMetal { get; set; }
        public bool HasWood { get; set; }
    }
}