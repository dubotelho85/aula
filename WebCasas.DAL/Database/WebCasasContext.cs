﻿using System.Data.Entity;

namespace WebCasas.DAL.Database
{
    public class WebCasasContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public WebCasasContext() : base("name=WebCasasContext")
        {
        }

        public System.Data.Entity.DbSet<WebCasas.Business.Entities.Door> Doors { get; set; }
    }
}
