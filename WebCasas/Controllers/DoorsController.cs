﻿using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebCasas.Business.Entities;
using WebCasas.DAL.Database;

namespace WebCasas.Controllers
{
    public class DoorsController : Controller
    {
        private WebCasasContext _webCasasContext;
        public DoorsController(WebCasasContext webCasasContext)
        {
            _webCasasContext = webCasasContext;
        }

        // GET: Doors
        public async Task<ActionResult> Index()
        {
            return View(await _webCasasContext.Doors.ToListAsync());
        }

        // GET: Doors/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Door door = await _webCasasContext.Doors.FindAsync(id);
            if (door == null)
            {
                return HttpNotFound();
            }
            return View(door);
        }

        // GET: Doors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Doors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Width,Height,MaterialName,HasMetal,HasWood")] Door door)
        {
            if (ModelState.IsValid)
            {
                _webCasasContext.Doors.Add(door);
                await _webCasasContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(door);
        }

        // GET: Doors/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Door door = await _webCasasContext.Doors.FindAsync(id);
            if (door == null)
            {
                return HttpNotFound();
            }
            return View(door);
        }

        // POST: Doors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Width,Height,MaterialName,HasMetal,HasWood")] Door door)
        {
            if (ModelState.IsValid)
            {
                _webCasasContext.Entry(door).State = EntityState.Modified;
                await _webCasasContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(door);
        }

        // GET: Doors/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Door door = await _webCasasContext.Doors.FindAsync(id);
            if (door == null)
            {
                return HttpNotFound();
            }
            return View(door);
        }

        // POST: Doors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            Door door = await _webCasasContext.Doors.FindAsync(id);
            _webCasasContext.Doors.Remove(door);
            await _webCasasContext.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
