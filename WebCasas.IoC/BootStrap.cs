﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCasas.Business.Services;
using WebCasas.Business.Services.Interfaces;
using WebCasas.DAL;
using WebCasas.DAL.Database;
using WebCasas.DAL.Interfaces;

namespace WebCasas.IoC
{
    public static class BootStrap
    {
        public static void Register(Container container)
        {
            container.Register<WebCasasContext>(Lifestyle.Scoped);
            //container.RegisterSingleton<ILog>(log4net.LogManager.GetLogger("name"));

            RegisterServices(container);
            RegisterDAL(container);
        }

        private static void RegisterServices(Container container)
        {
            container.Register<IDoorService, DoorService>();
            container.Register<IFloorService, FloorService>();
            container.Register<IHomeService, HomeService>();
            container.Register<IWindowService, WindowService>();
        }

        private static void RegisterDAL(Container container)
        {
            container.Register<IDoorDAL, DoorDAL>();
            container.Register<IFloorDAL, FloorDAL>();
            container.Register<IHomeDAL, HomeDAL>();
            container.Register<IWindowDAL, WindowDAL>();
        }
    }
}
